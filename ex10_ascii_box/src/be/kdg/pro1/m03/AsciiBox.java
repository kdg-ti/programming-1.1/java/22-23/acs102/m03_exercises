package be.kdg.pro1.m03;

import java.util.Scanner;

public class AsciiBox {
  public static void main(String[] args) {
    Scanner kbd = new Scanner(System.in);
    System.out.print("Enter the character do you want to draw: ");
    String character = kbd.nextLine();
    System.out.print("Enter the width : ");
    int width = kbd.nextInt();
    System.out.print("Enter the height : ");
    int height = kbd.nextInt();
    int x = 0;

    System.out.println("print line with while");
    while (x++ < width) {
      System.out.print(character);
    }
    System.out.println();

    System.out.println("print line with do-while");
    x = 0;
    do {
      System.out.print(character);
    } while (++x < width);
    System.out.println();


    System.out.println("print line with for");
    for (int i = 0; i < width; i++) {
      System.out.print(character);
    }
    System.out.println();

    System.out.println("print box with for");
    for (int y = 0; y < height; y++) {
      for (x = 0; x < width; x++) {
        System.out.print(character);
      }
      System.out.println();
    }

    System.out.println("print box with String.repeat");
    for (int y = 0; y < height; y++) {
      System.out.println(character.repeat(width));
    }


  }
}
