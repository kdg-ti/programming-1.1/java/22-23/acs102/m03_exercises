import java.util.Scanner;

public class Months {
  public static void main(String[] args) {
    System.out.print("Enter a month number (1 = January): ");
    Scanner keyboard = new Scanner(System.in);
    int month = keyboard.nextInt();
    //    if (month < 1 || month > 12){
//      System.out.println(month + " is not a valid month number.");
//    } else {
//      if (month ==1 || month==3||month==5||month==7||month==8||month==10||month==12 ){
//        System.out.println("Month " + month + " has " + bigMonth + " days.");
//      } else {
//        if (month == 2){
//          System.out.println("Month " + month + " has " + exceptionMonth + " days.");
//        } else {
//          System.out.println("Month " + month + " has " + smallMonth + " days.");
//        }
//      }
//    }
    int daysInMonth=0;
    if (month < 1 || month > 12) {
      System.out.println(month + " is not a valid month number.");
      return;
    }
    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 ||
      month == 12) {
      daysInMonth= 31;
    } else if (month == 2) {
      daysInMonth= 28;
    } else {
      daysInMonth= 30;
    }
    System.out.println("Month " + month + " has " + daysInMonth + " days.");
  }
}
