import java.util.Scanner;

public class MonthsSwitch {
  public static void main(String[] args) {
    System.out.print("Enter a month number (1 = January): ");
    Scanner keyboard = new Scanner(System.in);
    int month = keyboard.nextInt();



    int daysInMonth=0;
    if (month < 1 || month > 12) {
      System.out.println(month + " is not a valid month number.");
      return;
    }
    String monthName;
    switch (month){
      case 1: monthName="January"; break;
      case 2: monthName="February"; break;
      case 3: monthName="March"; break;
      case 4: monthName="April"; break;
      case 5: monthName="May"; break;
      case 6: monthName="June"; break;
      case 7: monthName="July"; break;
      case 8: monthName="August"; break;
      case 9: monthName="September"; break;
      case 10: monthName="October"; break;
      case 11: monthName="November"; break;
      case 12: monthName="December"; break;
      default: monthName="Unknown";
    }
//    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 ||
//      month == 12) {
//      daysInMonth= 31;
//    } else if (month == 2) {
//      daysInMonth= 28;
//    } else {
//      daysInMonth= 30;
//    }
    switch(month){
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12: daysInMonth=31;break;
      case 2:daysInMonth=28;break;
      default: daysInMonth=30;
    }
    System.out.println( monthName + " has " + daysInMonth + " days.");
  }
}
