import java.util.Scanner;

public class MonthsSwitchExpression {
  public static void main(String[] args) {
    System.out.print("Enter a month number (1 = January): ");
    Scanner keyboard = new Scanner(System.in);
    int month = keyboard.nextInt();



    int daysInMonth=0;
    if (month < 1 || month > 12) {
      System.out.println(month + " is not a valid month number.");
      return;
    }
    String monthName = switch (month) {
      case 1 -> "January";
      case 2 -> "February";
      case 3 -> "March";
      case 4 -> "April";
      case 5 -> "May";
      case 6 -> "June";
      case 7 -> "July";
      case 8 -> "August";
      case 9 -> "September";
      case 10 -> "October";
      case 11 -> "November";
      case 12 -> "December";
      default -> "Unknown";
    };
    //    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 ||
//      month == 12) {
//      daysInMonth= 31;
//    } else if (month == 2) {
//      daysInMonth= 28;
//    } else {
//      daysInMonth= 30;
//    }
    daysInMonth = switch (month) {
      case 1, 3, 5, 7, 8, 10, 12 -> 31;
      case 2 -> 28;
      default -> 30;
    };
    System.out.println( monthName + " has " + daysInMonth + " days.");
  }
}
